#!/bin/sh

set -e
cmd="$@"
until django-admin dbshell -- --command '\q'; do
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 5
done
django-admin migrate -v 2
django-admin collectstatic -v 2 --no-input
exec $cmd