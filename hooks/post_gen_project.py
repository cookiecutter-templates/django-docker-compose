#!/usr/bin/env python3

import sys, os

print(os.getcwd())
exit()

os.system("docker-compose build")
os.system("docker-compose run --rm web django-admin.py startproject {{ cookiecutter.app_name }} .")
os.system("docker-compose run --rm web python manage.py startapp {{ cookiecutter.project_name }}")
os.mkdir('{{ cookiecutter.project_name }}/static')

file = open("{{cookiecutter.app_name}}/settings.py", "r")
from_text = """        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),"""
to_text = """        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': '{{cookiecutter.project_name}}-db',
        'PORT': 5432,"""
content = file.read().replace(from_text, to_text)
file.close()

from_text = "STATIC_URL = '/static/'"
to_text = """STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '{{ cookiecutter.project_name }}/static')

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'"""
content = content.replace(from_text, to_text)

from_text = "INSTALLED_APPS = ["
to_text = """INSTALLED_APPS = [
    '{}',""".format("{{ cookiecutter.project_name }}")
content = content.replace(from_text, to_text)

file = open("{{cookiecutter.app_name}}/settings.py", "w")
file.write(content)
file.close()

os.system("docker-compose up -d && docker-compose run --rm web python manage.py createsuperuser && docker-compose down")
